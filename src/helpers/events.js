/**
 * Custom event identifiers
 * @module events
 */

const events = {
  layerAdd: 'layer-add',
  layerDelete: 'layer-delete',
  layerSelect: 'layer-select',
  layerToggleVisibility: 'layer-toggle-visibility',
  filtersApply: 'filters-apply',
  imageSelect: 'image-select',
  dataImported: 'data-imported',
  error: 'error'
}

export default events
