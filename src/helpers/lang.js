/**
 * Language strings
 * @module lang
 */

import {description} from '../../package'

const lang = {
  importImageOrData: 'Open image or data',
  importLocalData: 'Open local image or previously exported JSON file',
  layer: 'Layer',
  layersPreview: 'Layers',
  layerId: 'Layer ID',
  saveImage: 'Save image',
  addLayer: 'Add layer',
  hideLayer: 'Hide layer',
  showLayer: 'Show layer',
  deleteLayer: 'Delete layer',
  exportData: 'Export the currently edited composite image as JSON file that you can import back later',
  importData: 'Import data',
  file: 'File',
  by: 'by',
  layers: 'Layers',
  filters: 'Filters',
  mixBlendMode: 'Mix blend mode',
  zoom: 'Zoom',
  importImage: 'Open local image',
  exportingImage: 'Exporting image. Please, wait...',
  selectLocalImageOrData: 'Select local image or previously exported data',
  importPaneInfo: `Click the button below to import local image or previously exported data from the app. Depending on your computer configuration, larger files might take longer to process.<br /><br />Ideally, the images you import should not be larger than 2-3 megabytes.<br /><br />Built with VueJs, ES6, JavaScript, CSS3 and HTML5 by <a href="https://wemakesites.net/" target="_blank">Martin Ivanov</a>.`,
  fileTypeNotSupported: (type, mimeTypes) => {
    return `The selected file type (${type}) is not supported. To use ${description}, try to import one of the following types: ${mimeTypes.join(', ')}.`
  },
  errorMessageJson: 'The JSON file you tried to import does not match the data, expected by the app. Please, try to import a JSON file you have previously exported from the app.'
}

export default lang
