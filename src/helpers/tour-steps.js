/**
 * Onboarding tour
 * @module tourSteps
 */

const tourSteps = [{
  target: '#tour-step-01',
  content: 'Editing tools pane',
  params: {
    placement: 'right'
  }
}, {
  target: '#tour-step-02',
  content: 'Import or export images of JSON files',
  params: {
    placement: 'bottom'
  }
}, {
  target: '#tour-step-03',
  content: 'Zoom in or zoom out the currently open image',
  params: {
    placement: 'bottom'
  }
}, {
  target: '#tour-step-04',
  content: 'Apply filters and blend modes to the currently selected layer',
  params: {
    placement: 'top'
  }
}, {
  target: '#tour-step-05',
  content: 'Add, remove or select layers',
  params: {
    placement: 'top'
  }
}, {
  target: '#tour-step-06',
  content: 'Click to add new layer',
  params: {
    placement: 'top'
  }
}, {
  target: '.images-list li:first-child',
  content: 'Composite image layer representation',
  params: {
    placement: 'bottom'
  }
}, {
  target: '.images-list li:first-child .layer-toggle-visibility',
  content: 'Toggle layer\'s visibility. Disabled when image consists of just one layer',
  params: {
    placement: 'top'
  }
}, {
  target: '.images-list li:first-child .layer-delete',
  content: 'Delete layer. Disabled when image consists of just one layer',
  params: {
    placement: 'top'
  }
}, {
  target: '.composite-image',
  content: 'The currently edited image',
  params: {
    placement: 'left'
  }
}, {
  target: '.layers-preview',
  content: 'Draggable layers navigator. You can select layers from here or view their individual filters and visibility settings',
  params: {
    placement: 'right'
  }
}, {
  target: '.social-bar',
  content: 'Please, share if you like CSS HDR',
  params: {
    placement: 'left'
  }
}]

export default tourSteps
