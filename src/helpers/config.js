/**
 * App configuration
 * @module config
 */

import {name, author, version, description} from '../../package'

const config = {
  name,
  author,
  version,
  description,
  exportFileName (extension = 'jpg') {
    return `css-hdr.${extension}`
  },
  localData: {
    layersPreviewCoords: []
  }
}

export default config
