/**
 * Supported mime-types for locally imported data
 * @module mimeTypes
 */

const mimeTypes = [
  'image/jpeg',
  'image/png',
  'image/png',
  'image/gif',
  'text/json',
  'application/json'
]

export default mimeTypes
