/**
 * Method mixins, used application-wide
 * @module methods
 */

import lang from './lang'
import config from './config'
import events from './events'

const methods = {
  methods: {
    /**
     * Set local data
     * @method setLocalData
     * @return {object}
     */
    setLocalData (data) {
      data = JSON.stringify(data)
      localStorage.setItem(config.name, data)
      return this.getLocalData()
    },
    /**
     * Get the locally stored data
     * @method getLocalData
     * @return {object}
     */
    getLocalData () {
      return JSON.parse(localStorage.getItem(config.name))
    },
    /**
     * Remove the locally stored data
     * @method removeLocalData
     * @return {void}
     */
    removeLocalData () {
      localStorage.removeItem(config.name)
    },
    /**
     * Select layer by index
     * @method selectLayer
     * @return {void}
     */
    selectLayer (index) {
      this.$emit(events.layerSelect, index)
    },
    /**
     * Return new Date() one month from now
     * @method oneMonthFromNow
     * @return {Date}
     */
    oneMonthFromNow () {
      return new Date(Date.now() + 2.628e+9)
    },
    /**
     * Capitalize the first letter of a string
     * @method capitalize
     * @param {number} index
     * @return {string}
     */
    firstLetterCapital (string) {
      if (typeof string !== 'string') {
        return ''
      }
      return string.charAt(0).toUpperCase() + string.slice(1)
    },
    /**
     * Find image by index
     * @method findImageByIndex
     * @param {number} index
     * @return {number}
     */
    findImageByIndex (index) {
      return this.images.findIndex(image => image.index === index)
    },
    /**
     * Generate app title by package.json fields
     * @method title
     * @return {string}
     */
    title () {
      return `${config.description} ${config.version} ${lang.by} ${config.author.split('<')[0].trim()}`
    },
    /**
     * Open url in a popup
     * @method popup
     * @param {string} url
     * @param {string} title
     * @param {number} width
     * @param {number} height
     * @return {object}
     */
    popup (url = window.location.href, title = '', width = 640, height = 480) {
      const left = (screen.width / 2) - (width / 2)
      const top = (screen.height / 2) - (height / 2)
      return window.open(url, title, `width=${width}, height=${height}, top=${top}, left=${left}, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no`)
    }
  }
}

export default methods
