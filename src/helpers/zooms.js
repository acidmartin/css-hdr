/**
 * Values for the zoom selector
 * @module zooms
 */

const zooms = [
  '0.10',
  '0.15',
  '0.25',
  '0.5',
  '0.75',
  '1',
  '1.25',
  '1.5',
  '1.75',
  '2'
]

export default zooms
