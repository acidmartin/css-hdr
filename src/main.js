/**
 * App main WebPack entry file
 * @module main
 */

import Vue from 'vue'
import App from './App'
import VueTour from 'vue-tour'
import 'vue-tour/dist/vue-tour.css'
import TableLayout from './components/table-layout/table-layout'
import Row from './components/table-layout/row'
import Cell from './components/table-layout/cell'
import Icon from './components/icon'

import events from './helpers/events'
import config from './helpers/config'
import methods from './helpers/methods'
import lang from './helpers/lang'

import ControlsBox from './components/controls-box'

Vue.config.productionTip = false

Vue.use(VueTour)

Vue.component('table-layout', TableLayout)
Vue.component('row', Row)
Vue.component('cell', Cell)
Vue.component('controls-box', ControlsBox)
Vue.component('icon', Icon)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    App
  },
  mixins: [
    methods
  ],
  template: '<App/>',
  data () {
    return {
      lang,
      config,
      touched: false,
      importPane: true,
      selectedLayer: null,
      selectedImage: null,
      editingAreaWidth: null
    }
  },
  watch: {
    selectedImage () {
      this.$emit(events.imageSelect, {
        selectedLayer: this.selectedLayer,
        selectedImage: this.selectedImage
      })
    }
  },
  mounted () {
    this.onMounted()
  },
  created () {
    this.onCreated()
  },
  methods: {
    /**
     * Handler for the created callback of the component
     * @method onCreated
     * @return {void}
     */
    onCreated () {
      if (!this.getLocalData()) {
        this.setLocalData(this.config.localData)
      }
    },
    /**
     * Handler for the mounted callback of the component
     * @method onMounted
     * @return {void}
     */
    onMounted () {
      document.title = this.title()
      document.querySelector('body').setAttribute('data-exporting', this.lang.exportingImage)
      this.editingAreaWidth = document.querySelector('.editing-area-wrapper').offsetWidth
    }
  }
})
